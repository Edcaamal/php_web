<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// Devolver los valores de sesión
$estado_session = session_status();
if($estado_session == PHP_SESSION_NONE)
{
    session_start();
}

if (isset($_SESSION['loggedUserName'])) {
    ?>
    <h3>Bienvenido | <?php echo $_SESSION["loggedUserName"]; ?></h3>
    <p>
    <div class="card-panel">
        "Los ordenadores se hacen cada vez más inteligentes. Los científicos dicen que pronto ellos serán capaces de hablarnos (y con 'ellos' me refiero a los ordenadores, dudo mucho que los científicos sean capaces de hablarnos)"
        -- Dave Barry
    </div>
    <br>
    <div class="card-panel">
        "He notado últimamente que el miedo paranoico hacia ordenadores inteligentes tomando el control del mundo ha desaparecido totalmente. Todo lo que puedo contar es que este hecho coincide con la aparición de MS-DOS"
        -- Larry DeLuca
    </div>
    <br>
    <div class="card-panel">
        "Preguntarse cuándo los ordenadores podrán pensar es como preguntarse cuándo los submarinos podrán nadar"
        -- Edsger W. Dijkstra        
    </div>
    <br>
    <div class="card-panel">
        "Es ridículo vivir 100 años y sólo ser capaces de recordar 30 millones de bytes. O sea, menos que un compact disc. La condición humana se hace más obsoleta cada minuto"
        -- Marvin Minsky        
    </div>

    </p>
    <?php
} else {
    ?>
    <p>
    <h3>Usted no se encuentra registrado en nuestros sistemas</h3>    
    <?php
}
?>