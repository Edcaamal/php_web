<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!-- Pie de Página -->
<footer class="page-footer grey lighten-3">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="blue-text">Footer Content</h5>
                <p class="black-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
            </div>
            <div class="col l4 offset-l2 s12">
                <h5 class="black-text">Links</h5>
                <ul>
                    <li><a class="black-text text-lighten-3" href="#!">Link 1</a></li>
                    <li><a class="black-text text-lighten-3" href="#!">Link 2</a></li>
                    <li><a class="black-text text-lighten-3" href="#!">Link 3</a></li>
                    <li><a class="black-text text-lighten-3" href="#!">Link 4</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container black-text">
            © 2021 Copyright UACAM | FI - ISC. PAW ...
            <a class="black-text text-lighten-4 right" href="#!">More Links</a>
        </div>
    </div>
</footer>      

