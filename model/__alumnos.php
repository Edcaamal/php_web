<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of alumnos
 *
 * @author edgar
 */
class alumnos {
    public $id;
    public $alumo;
    public $nombre;
    public $sexo;
    
    public function __construct($id, $alumno, $nombre, $sexo) {  
        $this->id = $id;
        $this->alumno = $alumno;
        $this->nombre = $nombre;
        $this->sexo = $sexo;
    }  
    
    public static function consultar() {
        //$mysqli = conectadb::dbmysql();
        $mysqli = conectadb::dbmysql();
        
        $consulta = "select * from alumnos";
        echo ('<br>');
        // echo ($consulta);
        $resultado = mysqli_query($mysqli, $consulta);
        if (!$resultado) {
            echo 'No pudo Realizar la consulta a la base de datos';
            exit;
        }
        $listaAlumnos = [];
        while ($alumno = mysqli_fetch_array($resultado)) {
            $listaAlumnos[] = new alumnos($alumno['id'], $alumno['alumno'],$alumno['nombre'], $alumno['sexo']);
        }
        
        $mysqli->close;
        return $listaAlumnos;
        
    }    
    
}
